#!/bin/bash 

set import bash from busybox
set import cpuinfo from /proc/cpuinfo
set import lscpu

red='\033[1;91m'; deep_green='\033[0;32m'; green='\033[1;92m'; yellow='\033[1;93m'; blue='\033[1;94m'; white='\033[1;97m'; stop='\e[0m';

function lscpuData() {
	printf "=======================\n"
	printf "$red" && printf "Basic Information$stop\n"
	printf "=======================\n"
	lscpu | grep 'Model name'
	lscpu | grep Architecture;
	lscpu | grep Vendor;
	printf "Number of thread:\t\t " && grep -c 'model name' /proc/cpuinfo;
	grep 'cpu cores' /proc/cpuinfo | uniq
	lscpu | grep 'Thread(s) per core'
	printf "=======================\n"
	printf "$red" && printf "Cache Memory$stop\n"
	printf "=======================\n"
	lscpu | grep 'cache:'
}

function dmidecodeData() {

	# need to install manually 
        # root permission required 

	printf "=======================\n"
	printf "$red" && printf "DMIDEcode Information$stop\n"
	printf "=======================\n"
	read -p "do you want to read dmicode info? (y/n) " dm
	if [[ $dm == 'y' ]]; then {
		sudo dmidecode -t 4
	}; elif [[ $dm == 'n' ]]; then {
		printf "$red" && printf "denied to read dmidecode $stop\n"
	}; fi
}

clear && printf "cpuinfo$green v0.2\n$red"; 
echo -e "\e[5m `figlet "CPUinfo"` $stop"; 
# printf "$stop" && printf "Created by$yellow Farhan Sadik$stop\n";

lscpuData
dmidecodeData
