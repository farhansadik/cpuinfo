# CPUinfo 
## get your cpu info using a single bash script 

### Requirments
 1. Linux Distro
 2. Package `dmidecode` `lscpu` 
 > NOTE: you'll have to install `dmidecode` manually

### How to use 
 1. Getting permission <br> 
     `chmod +x cpuinfo.bash`
 2. Run <br>
     `./cpuinfo.bash`
> NOTE : `dmidecode` is required root permission

### Development by,
**Farhan Sadik** <br>
*_Square Development Group_*